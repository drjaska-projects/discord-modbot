# A Bot for Spam Bots

Annoyed by your Discord server's users falling for phishing and spamming
your server full of phishing links while you're asleep? This bot solves
the current situation by kicking any user who sends "offending" messages
to at least 3 channels with the messages containing `@everyone` or/and
`@here` mentions, regardless if they ping of not!

Users who send 3 or more offending messages to unique channels are
temporarily kicked, their messages are removed from the last 24 hours,
and they receive a DM from the bot notifying them about the kick and
mentioning that their account is likely compromised.

## Privacy

"[Message Content Intent](https://support-dev.discord.com/hc/en-us/articles/4404772028055-Message-Content-Privileged-Intent-FAQ)"
is required to see the contents of any message which doesn't ping it
or DM it, meaning that without "message intent" this bot couldn't see who
falls victim to phishers on servers which have `@everyone` pings disabled.

The only other two required permissions for this bot are permissions
to ban normal members, optionally the permission to send messages for those
who want a message containing "R.I.P. <username>" whenever a ban is applied.

This bot keeps no database or storage, it runs entirely in RAM meaning
that it does not keep chat logs. It only has a list of messages which
ping `@everyone` or/and `@here` and which are newer than 180 seconds and
the message only contains a timestamp and the ID of the user who sent
it, nothing else. When a user does send an offending message it does
print who sent the message and the message contents to standard output
stream meaning that who is running this bot is able to keep logs of the
program output but not guaranteed to.

## How to host on Linux/macOS/etc.

Dependencies:

- python3
- A bot token from Discord Developer Portal placed in .env

Optional dependencies:

- python3-pip
- python3-venv
- shell capable of sourcing/activating virtual environments

```sh
python3 -m venv .venv

. .venv/bin/activate

pip3 install -r requirements.txt

python3 bot.py
```

## License

GPL

## Author

Discord: @drjaska

Matrix: @drjaska:hacklab.fi
