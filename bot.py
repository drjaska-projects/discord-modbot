#!/usr/bin/env python3

import os
import time
import discord
from discord.errors import Forbidden
from dotenv import load_dotenv

from collections import namedtuple
mess = namedtuple("mess", "time authorid channelid")
messages = []

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)


@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!\n  Connected guilds:')
    for guild in client.guilds:
        print('    ' + guild.name)
    print()


# delete messages older than 180s from the list
def clear_old_messages():
    global messages
    cur_time = time.time()
    index = 0
    for message in messages:
        if cur_time - message.time < 180:
            # we found a message which is new, yeet all old ones before it
            messages = messages[index:]
            return
        else:
            index += 1
    # no new messages found at all, clear all old ones
    messages = []


def count_messages_of_user(id):
    global messages
    count = 0
    channels = []
    for message in messages:
        if message.authorid == id:
            if message.channelid not in channels:
                channels.append(message.channelid)
                count += 1
    return count


def format_dm_notification(guildname, content):
    return f"""
You have been temporarily kicked from {guildname} due to spam protection.
Your account is likely compromised as it is sending weird messages:

{content}
"""


@client.event
async def on_message(newmessage):
    if newmessage.author == client.user:
        return

    if str(newmessage.content).__contains__('@everyone') or \
       str(newmessage.content).__contains__('@here'):

        guild = newmessage.guild
        author = newmessage.author
        channel = newmessage.channel
        content = newmessage.content

        clear_old_messages()

        authorid = author.id
        messages.append(mess(time=time.time(),
                             authorid=authorid,
                             channelid=channel.id))

        mess_count = count_messages_of_user(authorid)

        print(author, 'sent #' + str(mess_count), 'bad message:', content)

        if mess_count > 2:
            print("Attempting to moderate")

            # DM the user to notify them of what has happened
            try:
                await author.send(format_dm_notification(
                                  guild,
                                  content))
                print("Sent DM to the user")
            except Forbidden:
                print("Could not send DM to the user")

            try:
                # kick and remove their messages from the last 1 days
                # 1day*24h=24h, 24h*60min=1440min, 1440min*60sec=86400sec
                await guild.ban(author,
                                delete_message_seconds=86400,
                                reason='spam bot detected')
                print("Banned the user")
            except Forbidden:
                print("Could not ban the user")

            time.sleep(10)  # load bearing sleep..

            try:
                await guild.unban(author,
                                  reason='let them rejoin')
                print("Unbanned the user")
            except Forbidden:
                print("Could not unban the user")

            try:
                await channel.send('R.I.P. ' + str(author))
                print("Sent public message to the channel")
            except Forbidden:
                print("Could not send public message to the channel")

            print('kicked', author,
                  'for "' + content + '"')


client.run(str(TOKEN))
